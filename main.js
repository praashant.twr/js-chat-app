displayChannels();
channel_id_element = null;
username_element = null;

function setUserName() {
    username_element = document.getElementById('user_name').value;
    if (username_element === "") {
        alert('Please enter a vaid username');
        return;
    }
    document.getElementById("selected-username").innerHTML = "user: " + username_element;
}

function addDataToChannelTable(channel_name) {
    fetch('http://0.0.0.0:5000/channels', {
        method: 'POST',
        body: JSON.stringify({
            name: channel_name
        })
    }).then(function(res) {
        console.log(res);
        return res.json();
    }).then(function(result) {
        console.log(result);
    });
}

function addDataToMessageTable(text_element) {
    if (channel_id_element === null) {
        alert('Please select a channel first');
        return;
    }
    if (username_element === null) {
        alert('Please select a username first');
        return;
    }
    fetch('http://0.0.0.0:5000/messages/', {
        method: 'POST',
        body: JSON.stringify({
            username: username_element,
            text: text_element,
            channel_id: channel_id_element
        })
    }).then(function(res) {
        console.log(res);
        return res.json();
    }).then(function(result) {
        console.log(result);
        broadcastMessages(result);
    });
}

function broadcastMessages(value) {
    console.log(value.text, value.username)
    fetch('http://0.0.0.0:5000/broadcast', {
        method: 'POST',
        body: JSON.stringify({
            data: value
        })
    }).then(function(res) {
        console.log(res);
        return res.json();
    }).then(function(result) {
        console.log(result);
    });
}

function displayChannels() {
    fetch('http://0.0.0.0:5000/channels').then(function(res) {
        console.log(res);
        return res.json();
    }).then(function(result) {
        let content = ''
        for (let i = 0; i < result.resources.length; i++) {
            let id = result.resources[i].id;
            const name = result.resources[i].name;
            content += `<li onclick = "displayMessages(${id},'${name}')">${name}</li>`;
        }
        document.getElementById("channels").innerHTML = content;
    });
}

function displayMessages(value, name) {
    channel_id_element = value;
    document.getElementById("channel-name").innerHTML = name;
    fetch(`http://0.0.0.0:5000/messages?channel_id=${channel_id_element}`).then(function(res) {
        console.log(res);
        return res.json();
    }).then(function(result) {
        console.log(result)
        content = ''
        for (let i = 0; i < result.resources.length; i++) {
            if (result.resources[i].channel_id === value) {
                content += "<b>" + result.resources[i].username + "</b>: " + result.resources[i].text + "<br>";
            }
        }
        document.getElementById('Channel-messages').innerHTML = content;
    });
}

function addNewChannel() {
    let value = document.getElementById("user_input").value;
    if (value === "") {
        alert('please enter a valid channel name');
        return;
    }
    var node = document.createElement("LI");
    var textnode = document.createTextNode(value);
    node.appendChild(textnode);
    document.getElementById("channels").appendChild(node);
    addDataToChannelTable(value);
}

function submitPreventDefault() {
    let btn = document.querySelector('#form-submit');
    btn.addEventListener('click', function(event) {
        event.preventDefault()
    });
}
submitPreventDefault();

function sendMessage() {
    let text_element = document.getElementById("text").value;
    addDataToMessageTable(text_element);
}

Pusher.logToConsole = true;

var pusher = new Pusher('45937e137aba113c2db2', {
    cluster: 'ap2',
    forceTLS: true
});

var channel = pusher.subscribe('messages');
channel.bind('message-added', function(data) {
    if (data.data.channel_id == channel_id_element) {
        content = "<div><b>" + data.data.username + "</b>: " + data.data.text + "</div>";
        document.getElementById('Channel-messages').innerHTML += content;
    }
});